import yaml
from pathlib import Path
import jinja2
import mistletoe
import re
import shutil
import hashlib
import requests
import zipfile
import io


class renderer(mistletoe.HTMLRenderer):
    def __init__(self, url, *extras):
        self.url = url
        super().__init__(*extras)

    def render_link(self, token):
        if token.target.startswith('https'):
            pass
        elif token.target.startswith('mailto'):
            pass
        else:
            token.target = f'{self.url}/{token.target}'

        return super().render_link(token)


def build_page(config, env, fn, prefixes):
    raw = fn.read_text()
    assert raw[:3] == '---'
    _, header, body = re.split('^---$', raw, flags=re.M)

    dic = yaml.safe_load(header)
    with renderer(config['url']) as r:
        dic['content'] = r.render(mistletoe.Document(body))

    dic['baseurl'] = config['url']
    if 'title' in dic:
        dic['pagetitle'] = f"{config['name']} &mdash; {dic['title']}"
    else:
        dic['pagetitle'] = f"{config['name']}"

    dic['gravatar'] = hashlib.md5(config['email'].lower().encode()).hexdigest()

    template = env.get_template(dic['template'] + '.html.jinja2')
    return template.render(page=dic, prefixes=prefixes)


def fetch_projects(projects, root, new_url):
    suffixes = [
        '.html', '.js', '.css'
    ]
    for project in projects:
        if ":" in project:
            proj, tag = project.split(":")
        else:
            proj = project
            tag = "root"


        ns, proj_name = proj.split("/")
        print(f"Fetching {proj_name}:{tag} (ns: {ns})")
        url = f"https://gitlab.com/api/v4/projects/{ns}%2F{proj_name}/jobs/artifacts/{tag}/download?job=pages"

        r = requests.get(url)
        assert r.ok

        z = zipfile.ZipFile(io.BytesIO(r.content))
        assert z.filelist[0].is_dir()
        proj_root = z.filelist[0].filename

        for file in z.filelist:
            if file.is_dir():
                continue

            if tag == "root":
                target =  root/proj_name/file.filename[len(proj_root):]
            else:
                target =  root/proj_name/tag/file.filename[len(proj_root):]

            target.parent.mkdir(parents=True, exist_ok=True)
            buf = z.read(file)

            if target.suffix in suffixes:
                if tag == "root":
                    buf = buf.decode().replace(
                        f"https://{ns}.gitlab.io/{proj_name}",
                        f"{new_url}/{proj_name}"
                    ).encode()
                else:
                    buf = buf.decode().replace(
                        f"https://{ns}.gitlab.io/{proj_name}",
                        f"{new_url}/{proj_name}/{tag}"
                    ).encode()

            target.write_bytes(buf)


def fontawesome(root, version="6.7.1", aiversion="1.9.4"):
    prefixes = {}

    r = requests.get(f"https://use.fontawesome.com/releases/v{version}/fontawesome-free-{version}-web.zip")
    assert r.ok
    z = zipfile.ZipFile(io.BytesIO(r.content))

    iconfiles = [
        ("css/brands.min.css", "fa-brands.css", "fa-brands"),
        ("css/solid.min.css", "fa-solid.css", "fa"),
        ("css/fontawesome.min.css", "fontawesome.css", "fa"),
        ("webfonts/fa-brands-400.woff2", "fa-brands-400.woff2", None),
        ("webfonts/fa-brands-400.ttf",   "fa-brands-400.ttf", None),
        ("webfonts/fa-solid-900.woff2",  "fa-solid-900.woff2", None),
        ("webfonts/fa-solid-900.ttf",    "fa-solid-900.ttf", None),
    ]
    for src, tgt, cls in iconfiles:
        dat = z.read(z.getinfo(f"fontawesome-free-{version}-web/{src}"))

        with open(root / tgt, "wb") as fp:
            fp.write(dat.replace(b"../webfonts/", b""))

        if cls:
            for icon in re.findall(b"\.(fa-[a-z\d-]*){", dat):
                prefixes[icon.decode()] = cls

    return prefixes


def academicons(root, version="1.9.4"):
    prefixes = {}

    r = requests.get(f"https://github.com/jpswalsh/academicons/archive/refs/tags/v{version}.zip")
    assert r.ok
    z = zipfile.ZipFile(io.BytesIO(r.content))

    iconfiles = [
        ("css/academicons.min.css", "academicons.css", "ai"),
        ("fonts/academicons.eot", "academicons.eot", None),
        ("fonts/academicons.svg", "academicons.svg", None),
        ("fonts/academicons.ttf", "academicons.ttf", None),
        ("fonts/academicons.woff", "academicons.woff", None),
    ]
    for src, tgt, cls in iconfiles:
        dat = z.read(z.getinfo(f"academicons-{version}/{src}"))

        with open(root / tgt, "wb") as fp:
            fp.write(dat.replace(b"../fonts/", b""))

        if cls:
            for icon in re.findall(b"\.(ai-[a-z\d-]*):", dat):
                prefixes[icon.decode()] = cls

    return prefixes


def build(config):
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(config['templates'])
    )
    out = config['output']

    try:
        out.mkdir()
    except FileExistsError:
        pass

    prefixes = fontawesome(out)
    prefixes.update(academicons(out))

    for page in config['pages'].iterdir():
        (out/page.with_suffix('.html').name).write_text(
            build_page(config, env, page, prefixes)
        )

    for fn in config['static'].iterdir():
        shutil.copy(fn, out/fn.name)

    fetch_projects(config['subprojects'], out, config['url'])


if __name__ == '__main__':
    import sys
    if len(sys.argv) == 1:
        domain = 'file'
    else:
        domain = sys.argv[1]

    config = {
        'url': '',
        'email': 'yannick.ulrich@durham.ac.uk',
        'name': 'Yannick Ulrich',
        'templates': Path('templates'),
        'output': Path('public'),
        'pages': Path('pages'),
        'static': Path('static'),
        'subprojects': [
            'yannickulrich/where-is-my-nobel-prize',
            'yannickulrich/loop-integration',
            'yannickulrich/loop-integration:2022',
            'yannickulrich/cv',
            'yannickulrich/phd-thesis',
            'yannickulrich/basic-qft',
            'yannickulrich/statistics-in-the-animal-kingdom',
            'yannickulrich/docker-talk',
            'yannickulrich/paradigms',
            'yannickulrich/workstop-5-talks',
        ]
    }

    domains = {
        'file': config['output'].resolve().as_uri(),
        'localhost': 'http://localhost:8000',
        'gitlab': 'https://yannickulrich.com',
    }

    config['url'] = domains[domain]

    build(config)
