---
title: Talks about coding
template: article
---
I have given a few formal talks in the IPPP Computing Club:
 * [git](https://gitlab.com/yannickulrich/git-tutorial)
 * [Binder](https://gitlab.com/yannickulrich/binder-demo)
 * [basic toolkits](https://gitlab.com/yannickulrich/computing-toolkit)
 * [Docker](https://yannickulrich.com/docker-talk)
 * [programming paradigms](https://gitlab.com/yannickulrich/paradigms)
