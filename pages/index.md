---
author: Yannick Ulrich
subtitle: Theoretical Physicist
template: title
social:
    fa-envelope: mailto:yannick.ulrich@liverpool.ac.uk
    fa-bluesky: https://bsky.app/profile/did:plc:megagzvvh7imufcruuruoany
    fa-gitlab: https://gitlab.com/yannickulrich
    fa-github: https://github.com/yannickulrich
    fa-linkedin: https://www.linkedin.com/in/yannick-ulrich-697637272
    ai-arxiv-square: http://arxiv.org/a/ulrich_y_1
    ai-inspire-square: https://inspirehep.net/authors/1599786
    ai-orcid-square: https://orcid.org/0000-0002-9947-3064
    ai-cv-square: https://yannickulrich.com/cv/
---

I'm a tenure-track lecturer in theoretical particle physics at the Department of Mathematical Sciences of the University of Liverpool.
I work on precision predictions for low-energy experiments.
You can read more about my research [here](research.html).

Some other things I was or am involved in:
 * [Teaching material](teaching.html)
 * [Outreach material by me and others](outreach.html)
 * Simulated cloud chamber for outreach
 * Formerly co-organiser of the [IPPP EDI group](https://www.ippp.dur.ac.uk/equity-diversity-and-inclusivity/)
 * [Computing talks](code-talks.html) for the IPPP Computing Club
 * [Advent of Code](https://gitlab.com/yannickulrich/aoc)
 * A [collection](linux.html) of Linux tools
