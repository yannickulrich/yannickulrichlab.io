---
title: Teaching material
template: article
---
Here are some assorted teaching materials I have prepared over time
 * a basic introduction to quantum field theory:
   [here](https://yannickulrich.com/basic-qft/qft.pdf)
 * a course on loop integration:
   [here](https://yannickulrich.com/loop-integration/)
 * notes for a short blackboard outreach talk on precision physics:
   [here](nuffield-research-placement.pdf)
 * an EDI lecture on under-represented and under-appreciated scientists
   [here](https://yannickulrich.com/where-is-my-nobel-prize/)
 * a summer school lecture on Monte Carlo generators
   [here](http://yannickulrich.gitlab.io/talks/24-09-06-mc-generators.pdf)
