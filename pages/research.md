---
title: Research interest
template: article
---
The Standard Model of particle physics (SM) is the most precise and
complete theory of fundamental particles and interactive.
To search for physics beyond this SM, physicists can search for small differences between the SM prediction and experiments.
To do this, we need to know the SM prediction very precisely.

My research is into providing precision predictions for low-energy experiments such as [MUonE](https://web.infn.it/MUonE/).
I am developing and maintaining the framework [McMule](https://mule-tools.gitlab.io/) to provide such predictions.

Recently I started working on predictions for *ee*&to;hadrons and am coordinating the [RadionMonteCarLow 2 effort](https://radiomontecarlow2.gitlab.io/)
