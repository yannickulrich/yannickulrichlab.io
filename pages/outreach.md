---
title: Outreach material
template: article
---
Here is some outreach material I have made myself

 * notes for a short blackboard outreach talk on precision physics:
   [here](nuffield-research-placement.pdf)
 * slides for school outreach for the Wardley STEM Week:
   [here](wardley-stem.pdf)

Here is some material I found useful

 * Lucy Budge's *Modelling the Invisible* slides:
   hosted [here](https://www.ippp.dur.ac.uk/~hhassan/Outreach/ModellingInvsible.pdf)
 * The NeutrinoScope App in the [Apple App Store](https://apps.apple.com/gb/app/neutrinoscope/id1384582896)
 * Visualising the Proton by the MIT and JLab on [YouTube](https://www.youtube.com/watch?v=Dt8FZ4ksWiY)
