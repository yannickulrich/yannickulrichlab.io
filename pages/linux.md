---
title: Linux phones
template: article
---
Here are a bunch of Linux tools I have written or contributed to
 * [Pinephone fingerprint sensor](https://gitlab.com/yannickulrich/pia): an implementation of the [PinePhone fingerprint sensor](https://wiki.pine64.org/wiki/PinePhone_(Pro)_Add-ons#Fingerprint_Reader_Add-on).
   There is also a patch to use this with [phosh](https://gitlab.gnome.org/World/Phosh/phosh)
 * [Pinephone Gestures](https://gitlab.com/yannickulrich/pine_gestures):
   original idea by [Zachary Schroeder](https://github.com/zschroeder6212/pine_gestures), this application allows you to launch applications (such as the flashlight) using IMU gestures.
 * [postprocessd daemon](https://gitlab.com/yannickulrich/postprocessd):
   postprocessd is a native pipeline for Megapixels [by Martijn Braam](https://git.sr.ht/~martijnbraam/postprocessd).
   This fork allows it to run as a daemon with a queue.
 * [oscclip](https://gitlab.com/yannickulrich/oscclip): a small tool to access the system clipboards from anywhere using the ANSI OSC52 sequence
 * [pylight](https://gitlab.com/yannickulrich/pylight): A minimal driver for the Logitech spotlight
